// objective 1
db.fruits.aggregate([
     {$match: {onSale: true}},
     {$count: "fruitsOnSale"}
])

// objective 2
db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {$count: "enoughStock"}
])

// objective 3

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
])

// objective 4

/*db.fruits.aggregate([
    {$match: {$or:[
        {supplier_id: 1},
        {supplier_id: 2}
        ]}},
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
])*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
])

// objective 5
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
])